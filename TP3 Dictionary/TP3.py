# -*- coding: utf-8 -*-
"""
Created on Tue Nov  5 07:40:50 2019

@author: risa
"""
import csv
import operator

# Opening Program
print('='*40)
print('************SELAMAT DATANG**************')
print('-----------BudayaKB Lite v1.0-----------')
print('='*40)
while True:
    opening = input('Tekan tombol Enter untuk memulai!')
    break
    
def Main():
    faq()
    try: 
        while True:
            kalimat = input('Masukan Perintah: ')
            kalimat = kalimat.split(' ',1)
            
            perintah = kalimat[0].lower()
            if len(kalimat) > 1:
                objek = kalimat[1].title()
                
            if perintah == 'impor' or perintah == '1':
                csv_dict = imporcsv(objek)
                
            elif perintah == 'carinama' or perintah == '3':
                carinama(objek, csv_dict)
                
            elif perintah == 'caritipe' or perintah == '4':
                caritiprov(objek, csv_dict)
                
            elif perintah == 'cariprov' or perintah == '5':
                caritiprov(objek, csv_dict)
                
            elif perintah == 'tambah' or perintah == '6':
                tambah(objek, csv_dict)
                
            elif perintah == 'update'or perintah == '7':
                update(objek, csv_dict)
                
            elif perintah == 'hapus' or perintah == '8':
                hapus(objek, csv_dict)
                
            elif perintah == 'stat' or perintah == '9':
                stat(csv_dict)
                
            elif perintah == 'stattipe'or perintah == '10':
                stattipe(csv_dict)
                
            elif perintah == 'statprov'or perintah == '11':
                statprov(csv_dict)
                
            elif perintah == 'ekspor' or perintah == '2':
                eksporcsv(objek, csv_dict)
                
            elif perintah == 'keluar' or perintah == '':
                print('='*40)
                print('************SELAMAT TINGGAL*************')
                print('='*40)
                print('---------------------------------(。･∀･)ﾉ')
                break
            
            elif objek == '*':
                continue
            
            elif perintah == '?' or perintah == 'faq':
                faq()
                
            else:
                print('Apakah kamu melakukan typo?')
                print('***PERINTAH TIDAK TERDEFINISI***')
            print('#'*4)
                  
    except UnboundLocalError:
        print('***PERINTAH TIDAK LENGKAP***')
        print('****SILAHKAN RUN LAGI****')
    except TypeError:
        print('****SILAHKAN RUN LAGI****')
    except IndexError:
        print('***PERINTAH TIDAK TERDEFINISI***')
        print('****SILAHKAN RUN LAGI****')

def faq():
    '''Mengeprint Instruksi Awal sebagai guide untuk user untuk
    menjalankan program'''
    print('')
    print('-------------Instruksi Awal-------------')
    print('  LIST PERINTAH YANG DAPAT KAMU GUNAKAN')
    print('-'*40)
    print('[1] IMPOR    [2] EKSPOR')
    print('[3] CARINAMA [4] CARITIPE   [5] CARIPROV')
    print('[6] TAMBAH   [7] UPDATE     [8] HAPUS')
    print('[9] STAT     [10] STATTIPE  [11] STATPROV')
    print('')
    print('TIPS') 
    print('> Setiap perintah memiliki shortcut berupa angka',
          'yang ada di Instruksi Awal!')
    print('> Selalu Impor file terlebih dahulu!')    
    print('> Input "faq" atau "?" untuk melihat Instruksi Awal!')
    print('> Input "keluar" atau tekan Enter bila kamu sudah selesai!')

def imporcsv(objek):
    '''Mengimpor File CSV'''
    try:
        with open(objek, mode='r') as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            csv_dict = {}
            for row in csv_reader:
                csv_dict[row[0]] = row[1:]
            print('Terdapat', len(csv_dict), 'baris')
            return csv_dict
        
    except FileNotFoundError:
        print('***FILE TIDAK DITEMUKAN***')
        print('Pastikan kamu sudah menyimpan filenya di folder yang sama!')
        pass
    except IndexError:
        print('***TOLONG INPUT FILE***')
        pass
    except TypeError:
        print('***TOLONG JANGAN ANEH-ANEH***')
        pass
    
def carinama(objek, csv_dict):
    '''Mencari nama warisan budaya dalam file csv'''
    if objek == '*':
        # Memasukan seluruh values yang berbentuk list dalam csv_dict 
        # dan mengubahnya menjadi bentuk string ke dalam value_list
        value_list = [','.join(map(str, values)) for values in csv_dict.values()] 
        count = 0
        for key in csv_dict.keys():
            print(key, ',', value_list[count], sep='')
            count += 1
    else:
        for key in csv_dict.keys():
            if objek in key:
                if objek == key:
                    value = ','.join(map(str, csv_dict[key]))
                    print(key + ',' + value)
                    break
            else:
                print(objek + ' tidak ditemukan :(')
                break
        
def caritiprov(objek, csv_dict): 
    '''Mencari nama tipe dan provinsi warisan budaya yang ada di file csv'''
    value_list = [] # list value yang ingin dicari keynya
    for values in csv_dict.values():
        if objek in values:
            value_list.append(values)
           
    key_list = [] # list kosong untuk menaruh key yang sedang dicari
    i = 0
    while i <= len(value_list)-1:
        for keys in csv_dict.keys():
            if csv_dict[keys] == value_list[i]:
                key_list.append(keys) 
        i += 1
    for element in key_list:
        value = ','.join(map(str, csv_dict[element]))
        print(element + ',' + value)
    print('*Ditemukan', len(key_list), 'warisan budaya*')

    
def tambah(objek, csv_dict):
    ''' Menambahkan warisan budaya baru ke csv_dict'''
    if ';;;' not in objek:
        print('***Objek yang ingin ditambah tidak sesuai format!***')
        print('FORMATNYA = namawarisanbudaya;;;tipe;;;provinsi;;;referenceurl')
    else:
        x = objek.split(';;;')
        if x[0] not in csv_dict:
            csv_dict[x[0]] = x[1:]
            print(x[0], 'ditambahkan')
        elif len(x) < 4:
             print('***Objek yang ingin ditambah tidak sesuai format!***')
             print('FORMATNYA = namawarisanbudaya;;;tipe;;;provinsi;;;referenceurl')
        else:
            print('***Warisan Budaya sudah ada di dalam file!***')
            print('Silahkan gunakan perintah "UPDATE" bila ingin mengubah')
    return csv_dict


def update(objek, csv_dict):
    ''' Mengupdate warisan budaya ke csv_dict'''
    if ';;;' not in objek:
        print('***Objek yang ingin diupdate tidak sesuai format!***')
        print('FORMATNYA = namawarisanbudaya;;;tipe;;;provinsi;;;referenceurl')
    else:
        x = objek.split(';;;')
        if len(x) < 4:
             print('***Objek yang ingin ditambah tidak sesuai format!***')
             print('FORMATNYA = namawarisanbudaya;;;tipe;;;provinsi;;;referenceurl')
        else:
            csv_dict[x[0]] = x[1:]
            print(x[0], 'diupdate')
    return csv_dict   

def hapus(objek, csv_dict):
    ''' Menghapus warisan budaya baru dalam csv_dict '''
    try: 
        del csv_dict[objek]
        print(objek, 'dihapus')
        return csv_dict
    except KeyError:
        print('***KEY TIDAK DITEMUKAN***')
        print('Nampaknya key yang ingin kamu hapus tidak ada!')
        pass
    
def stat(csv_dict): 
    ''' Mengecek jumlah warisan budaya yang telah di impor dan modifikasi '''
    print('Terdapat', len(csv_dict), 'warisan budaya')

def stattipe(csv_dict):
    ''' Mengeprint tipe-tipe warisan budaya yang ada didalam file dan 
    menghitung jumlah per tiap tipe yang ada di dalam fie '''
    tipe = [] # List berisi tipe warisan budaya yang tidak berulang
    tipe_all = [] # List berisi tipe warisan budaya yang masih berulang
    count = [] # List berisi frekuensi jumlah tiap tipe
    for values in csv_dict.values():
        tipe_all.append(values[0])
        if values[0] not in tipe: # Menyortir 
            tipe.append(values[0])
            
    # Menghitung frekuensi tiap tipe
    count = [tipe_all.count(tipe[i]) for i in range(len(tipe))]
    # Menggabungkan 2 list dan menjadikannya dictionary(key,value)
    d1 = dict(zip(tipe, count)) 
    # Menyortir dictionary dari yang memiliki value terbesar
    x = sorted(d1.items(), key=operator.itemgetter(1), reverse=True)
    print(x)
    return x
    
def statprov(csv_dict):
    ''' Mengeprint provinsi-provinsi warisan budaya yang ada didalam file dan 
    menghitung jumlah per tiap provinsi yang ada di dalam fie '''
    prov = [] # List berisi provinsi tiap warisan budaya yang tidak berulang
    prov_all = [] # List berisi provinsi tiap warisan budaya yang masih berulang
    count = [] # List berisi frekuensi jumlah tiap provinsi
    
    for values in csv_dict.values():
        prov_all.append(values[1])
        if values[1] not in prov:
            prov.append(values[1])
    count = [prov_all.count(prov[i]) for i in range(len(prov))]
    d1 = dict(zip(prov, count))
    y = sorted(d1.items(), key=operator.itemgetter(1), reverse=True)
    print(y)
    return y

def eksporcsv(objek, csv_dict):
    '''Mengekspor file berisikan file yang telah dimodifikasi dalam program'''
    try:
        with open(objek, mode='w') as csv_file2:
            csv.writer(csv_file2, delimiter=',')
            for k, v in csv_dict.items():
                csv_file2.write(k + ',' + ','.join(v) + '\n')
            csv_file2.close()
            print('Terekspor', len(csv_dict), 'baris')
    except OSError:
        print('***FILE TIDAK DAPAT DIEKSPOR***')
        print('Penamaan file tidak valid!')
        pass
    
if __name__ == "__main__":
    Main() 