#!/usr/bin/env python3
"""
TEMPLATE TP4 DDP1 Semester Gasal 2019/2020
Author: 
Ika Alfina (ika.alfina@cs.ui.ac.id)
Evi Yulianti (evi.yulianti@cs.ui.ac.id)
Meganingrum Arista Jiwanggi (meganingrum@cs.ui.ac.id)
Last update: 26 November 2019
"""
from budayaKB_model import BudayaItem, BudayaCollection
from flask import Flask, request, render_template
# from wtforms import Form, validators, TextField

app = Flask(__name__)
app.secret_key ="tp4"

#inisialisasi objek budayaData
databasefilename = ""
budayaData = BudayaCollection()


#merender tampilan default(index.html)
@app.route('/')
def index():
	return render_template("index.html")

# Bagian ini adalah implementasi fitur Impor Budaya, yaitu:
# - merender tampilan saat menu Impor Budaya diklik	
# - melakukan pemrosesan terhadap isian form setelah tombol "Import Data" diklik
# - menampilkan notifikasi bahwa data telah berhasil diimport 	
@app.route('/imporBudaya', methods=['GET', 'POST'])
def importData():
	if request.method == "GET":
		return render_template("imporBudaya.html")

	elif request.method == "POST":
		try:
			f = request.files['file']
			global databasefilename
			databasefilename=f.filename
			jumlah_baris = budayaData.importFromCSV(f.filename)
			n_data = len(budayaData.koleksi) #jumlah data yang sudah terdapat dalam koleksi
			return render_template("imporBudaya.html", result=n_data, fname=f.filename, jumlah=jumlah_baris)
		except FileNotFoundError:
			return render_template("imporBudaya.html", result=-1, fname=f.filename)


@app.route('/resetBudaya', methods=['GET'])
def resestBudaya():
	hapus = budayaData.koleksi.clear()
	return render_template("index.html", hapus=hapus)


@app.route('/tambahBudaya', methods=['GET', 'POST'])
def tambahBudaya():
	if request.method == "POST":
		result_tambah = budayaData.tambah(request.form['nama'], request.form['tipe'], request.form['provinsi'], request.form['url'])
		return render_template("tambahBudaya.html", result=result_tambah, result_nama=request.form['nama'])
	else:
		return render_template("tambahBudaya.html")


@app.route('/ubahBudaya', methods=['GET', 'POST'])
def ubahBudaya():
	if request.method == "POST":
		result_ubah = budayaData.ubah(request.form['nama'], request.form['tipe'], request.form['provinsi'], request.form['url'])
		return render_template("ubahBudaya.html", result=result_ubah, result_nama=request.form['nama'])	
	else:
		return render_template("ubahBudaya.html")

@app.route('/hapusBudaya', methods=['GET', 'POST'])	
def hapusBudaya():
	if request.method == "POST":
		result_hapus = budayaData.hapus(request.form['nama'])
		return render_template("hapusBudaya.html", result=result_hapus, result_nama=request.form['nama'])		
	else:
		return render_template("hapusBudaya.html")


@app.route('/cariBudaya', methods=['GET', 'POST'])
def cariBudaya():
	if request.method == "GET":
		return render_template("cariBudaya.html")
	else:
		if 'Nama' == request.form['cari']: #Mengakses form select bernama 'cari' di file cariBudaya.html
			# request.form['teks'] berfungsi untuk mengakses inputan user berupa teks di file cariBudaya.html
			# dan menjadikannya parameter untuk fungsi carinama/tipe/prov
			result_cari = budayaData.cariByNama(request.form['teks']) 
		elif 'Tipe' == request.form['cari']:
			result_cari = budayaData.cariByTipe(request.form['teks'])
		elif 'Asal Provinsi' == request.form['cari']:
			result_cari = budayaData.cariByProv(request.form['teks'])
		return render_template("cariBudaya.html", result=result_cari, jumlah=len(result_cari), jenis=request.form['cari'], teks=request.form['teks'])


@app.route('/statsBudaya', methods=['GET', 'POST'])	
def statsBudaya():
	if request.method == "POST":
		if 'All' == request.form['stat']:
			result_stat = budayaData.stat()
			return render_template("statsBudaya.html", result=result_stat, result_nama=request.form['stat'])
		else:
			if 'Tipe' == request.form['stat']:
				result_stat = budayaData.statByTipe()
			elif 'Asal Provinsi' == request.form['stat']:
				result_stat = budayaData.statByProv()
			return render_template("statsBudaya.html", result=result_stat, result_nama=request.form['stat'], jumlah = len(result_stat))
	else:
		return render_template("statsBudaya.html")
	

# run main app
if __name__ == "__main__":
	app.run(debug=True)