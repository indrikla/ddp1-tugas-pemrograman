import string

def InputText():
    ''' Menginput pesan berulang kali 
    dan memecah kalimat(str) menjadi kata(list)'''
    
    print('='*55)
    print("Masukan Pesan : (untuk berhenti masukan string kosong)")
    print('='*55)
    
    pesan_list = []
    while True:
        pesan_str = input("Pesan: ")
        # Menggunakan .lower() agar input kapital dan non dianggap sama
        pesan_str = pesan_str.lower()
        
        if pesan_str == '':
            break # Program akan berhenti menerima input dari user

        for kata in pesan_str.split(' '):
            pesan_list.append(kata)
    return pesan_list


def RemovePunc():
    ''' Menghilangkan tanda baca dalam list pesan'''
    
    pesan_list = InputText()
    p2 = []
    tanda_baca = string.punctuation.replace('-','')
    
    for kata in pesan_list:
        p = ''
        
        for char in kata:
            if char in tanda_baca:
                kata = kata.strip(tanda_baca)
            else:
                p = p + char
        p2.append(p)
        
    return p2


def RemovePunc2():
    ''' Menghilangkan string kosong 
    dan tanda "-" dalam list pesan'''
    
    pesan_clean = RemovePunc()
    while '' in pesan_clean:
        pesan_clean.remove('')
    while '-' in pesan_clean:
        pesan_clean.remove('-')
        
    return pesan_clean
        

def StopWord():
    ''' Membuka file stopword'''
    
    file = open('TP2-stopword.txt', 'r')
    file_list = []
    
    for lines in file.read().split():
        file_list.append(lines)
    return file_list


def RemoveSW():
    ''' Menghilangkan stopword dalam list pesan'''
    
    konjungsi_list = StopWord()
    pesan_clean = RemovePunc2()
    hasil = []
    
    for kata in konjungsi_list:
        for kata in pesan_clean:
            if kata in konjungsi_list:
                pesan_clean.remove(kata)
                
    hasil.extend(pesan_clean)
    return hasil


def Frekuensi():
    ''' Menghitung frekuensi per kata dalam list pesan'''
    
    pesan_clean = RemoveSW()
    kata_list = []
    frekuensi_list = []
    
    for char in pesan_clean:
        if char not in kata_list:
            kata_list.append(char)
            frekuensi_list.append(pesan_clean.count(char))
            
    return (kata_list, frekuensi_list)
        

def Tabel():
    ''' Membuat tabel berisikan nomor, kata, dan frekuensi
    dari data list pesan'''
    
    print('Distribusi frekuensi kata:')
    print('-'*30)
    print('{} {:>5} {:>16}'.format ('No', 'Kata', 'Frekuensi'))
    print('-'*30)
    
    for index in range (len(k)):
        print('{:3} {:<15} {:<10}'.format ((index+1), k[index], f[index]))
        
        
def Grafik():
    ''' Membuat grafik batang dari data list pesan
    yang sebelumnya sudah dibuat dalam bentuk tabel'''
    
    import matplotlib.pyplot as plt
    
    fig, ax = plt.subplots(figsize=(7,15))
    ax.barh(k, f)        
    ax.set(xlabel='Frekuensi', title='Frekuensi Kemunculan Kata')
    plt.gca().invert_yaxis()


# Pemanggil Fungsi

# Penggunaan var k (kata_list) dan var f (frekuensi_list) 
# untuk mempersimpel penulisan
k,f = Frekuensi()
Tabel()
Grafik()
